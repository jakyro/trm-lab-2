﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Singleton : MonoBehaviour
{
    private static Singleton instance;

    public static Singleton Instance
    {
        get { return instance ?? (instance = new GameObject("Singleton").AddComponent<Singleton>()); }
    }

    private GameObject choice1;
    private GameObject choice2;

    public void TargetFound(GameObject obj)
    {
        Debug.Log("Found: " + obj.name);
        if (obj.name.EndsWith("1"))
        {
            //DeactivateExcept(obj.name);
            choice1 = obj;
        }
        if (obj.name.EndsWith("2"))
        {
            //DeactivateExcept(obj.name);
            choice2 = obj;
        }

        if (choice1 != null && choice2 != null)
        {
            TryGame();
        }
    }

    public void TargetLost(GameObject obj)
    {
        Debug.Log("Lost: " + obj.name);
        if (obj.name.EndsWith("1"))
        {
            choice1 = null;
            //ActivateAll("1");
        }
        if (obj.name.EndsWith("2"))
        {
            choice2 = null;
            //ActivateAll("2");
        }
        SetText("Choose");
    }

    private void TryGame()
    {

        string s1 = choice1.name.Remove(choice1.name.Length - 1, 1);
        string s2 = choice2.name.Remove(choice2.name.Length - 1, 1);
        Debug.Log(s1 + "-" + s2);
        if (s1 == s2)
        {
            SetText("TIE!");
            return;
        }
        else if (
            s1 == "Rock" && s2 == "Scissor" ||
            s1 == "Scissor" && s2 == "Paper" ||
            s1 == "Paper" && s2 == "Rock")
        {
            SetText("Player 1 Wins!");
        }
        else
        {
            SetText("Player 2 Wins!");
        }
    }

    private void SetText(string str)
    {
        GameObject text = GameObject.Find("Text");
        TextMesh mesh = text.GetComponentInChildren<TextMesh>() as TextMesh;
        mesh.text = str;
    }

    private void DeactivateExcept(string name)
    {
        string idx = name.Substring(name.Length - 1);
        GameObject holder = GameObject.Find("Holder");
        GameObject rock = holder.transform.Find("Rock" + idx).gameObject;
        GameObject scissor = holder.transform.Find("Scissor" + idx).gameObject;
        GameObject paper = holder.transform.Find("Paper" + idx).gameObject;
        rock.SetActive(rock.name == name);
        scissor.SetActive(scissor.name == name);
        paper.SetActive(paper.name == name);
    }

    private void ActivateAll(string idx)
    {
        GameObject holder = GameObject.Find("Holder");
        GameObject rock = holder.transform.Find("Rock" + idx).gameObject;
        GameObject scissor = holder.transform.Find("Scissor" + idx).gameObject;
        GameObject paper = holder.transform.Find("Paper" + idx).gameObject;
        rock.SetActive(true);
        scissor.SetActive(true);
        paper.SetActive(true);
    }
}
