﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectBehavior : MonoBehaviour
{
    public void TargetFound()
    {
        Singleton.Instance.TargetFound(gameObject);
    }

    public void TargetLost()
    {
        Singleton.Instance.TargetLost(gameObject);
    }
}
