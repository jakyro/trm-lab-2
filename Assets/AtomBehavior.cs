﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AtomBehavior : MonoBehaviour
{
    public void TargetFound()
    {
        Chemistry.Instance.TargetFound(gameObject);
    }

    public void TargetLost()
    {
        Chemistry.Instance.TargetLost(gameObject);
    }
}
