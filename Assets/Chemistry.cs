﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chemistry : MonoBehaviour
{
    private static Chemistry instance;

    public static Chemistry Instance
    {
        get { return instance ?? (instance = new GameObject("Chemistry").AddComponent<Chemistry>()); }
    }

    private List<GameObject> hydrogens = new List<GameObject>();
    private List<GameObject> oxygens = new List<GameObject>();
    private bool isH20 = false;

    public void TargetFound(GameObject obj)
    {
        if (obj.tag == "H")
        {
            hydrogens.Add(obj);
            Debug.Log("Hydrogen Found: " + hydrogens.Count);
        }
        if (obj.tag == "O")
        {
            oxygens.Add(obj);
            Debug.Log("Oxygen Found: " + oxygens.Count);
        }
        TestFormula();
    }

    public void TargetLost(GameObject obj)
    {
        if (obj.tag == "H")
        {
            SetObjectStateH(obj, true);
            hydrogens.Remove(obj);
            Debug.Log("Hydrogen Lost: " + hydrogens.Count);
        }
        if (obj.tag == "O")
        {
            SetObjectStateO(obj, false);
            oxygens.Remove(obj);
            Debug.Log("Oxygen Lost: " + oxygens.Count);
        }
        TestFormula();
    }

    private void TestFormula()
    {
        if (hydrogens.Count == 2 && oxygens.Count == 1)
        {
            if (!isH20)
            {
                Assemble();
            }
        }
        else
        {
            Disassemble();
        }
    }

    private void Assemble()
    {
        isH20 = true;
        SetObjectStateH(hydrogens[0], false);
        SetObjectStateH(hydrogens[1], false);
        SetObjectStateO(oxygens[0], true);
    }

    private void Disassemble()
    {
        isH20 = false;
        hydrogens.ForEach(delegate (GameObject h)
        {
            SetObjectStateH(h, true);
        });
        oxygens.ForEach(delegate (GameObject o)
        {
            SetObjectStateO(o, false);
        });
    }

    private void SetObjectStateH(GameObject h, bool state)
    {
        h.transform.Find("Sphere").gameObject.SetActive(state);
    }

    private void SetObjectStateO(GameObject o, bool state)
    {
        o.transform.Find("OxyH1").gameObject.SetActive(state);
        o.transform.Find("OxyH2").gameObject.SetActive(state);
    }
}
